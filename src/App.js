import React, { Fragment } from 'react';
import logo from './logo.svg';
import './App.css';
import { Link, Route } from 'react-router-dom';
import { HomeLinks, HomeContainer } from './styled/App';
import Country from './components/Country';

function App({ match }) {
  const code = 'AQ';
  return (
    <Fragment>
      <HomeContainer>
        <HomeLinks to='/countries'>Countries</HomeLinks>
        <HomeLinks to={`/countries/${code}`}>Country</HomeLinks>
      </HomeContainer>
      <Route path={`/countries/:code`} component={Country} />
    </Fragment>
  );
}

export default App;
