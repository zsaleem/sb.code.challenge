import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  body {
    background: #ecf1f7;
  }
  .overflow {
    overflow: hidden;
  }
`;
