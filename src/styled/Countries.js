import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Back = styled(Link)`
  display: block;
  padding: 1em;
  color: #000;
  text-decoration: none;
  font-weight: bolder;
  width: 2em;
`;

export const Items = styled.ul`
  width: 20em;
  list-style-type: none;
  padding: 0;
  margin: 0;
  background: #f7f8fa;
  border-radius: 1em;
  padding: 1em;
  display: inline-block;
  margin-right: 1em;
  margin-bottom: 1em;
  vertical-align: top;
`;

export const Title = styled.h4`
  padding: 0;
  margin: 0;
  padding: .5em .5em 1em;
`;

export const Continent = styled.li`
  border-radius: 1em;
  overflow: scroll;
  height: 500px;
  &.antarctica {
    height: auto;
  }
`;

export const CountriesContainer = styled.div`
  background-color: #ecf1f7;
  padding: 1em;
`;

export const Links = styled(Link)`
  text-decoration: none;
  color: #262627;
  font-size: .9em;
  width: 100%;
  display: block;
`;

export const CountryName = styled.span`
  font-weight: bold;
`;