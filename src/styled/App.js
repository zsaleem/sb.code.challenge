import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const HomeContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  text-align: center;
  height: 20em;
`;

export const HomeLinks = styled(Link)`
  display: inline-block;
  padding: 5em;
  color: #000;
  text-decoration: none;
  font-weight: bolder;
  box-shadow: 0 0 .1em #ccc;
  margin-right: 1em;
  :hover {
    box-shadow: 0 0 .5em #ccc;
  }
`;