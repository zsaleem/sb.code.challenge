import styled from 'styled-components';

export const Loading = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  width: 20em;
  height: 20em;
  text-align: center;
  font-weight: bold;
  width: 100%;
`;

export const Failed = styled(Loading)`
  z-index: 11;
`;