import styled from 'styled-components';

export const Overlay = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: #000;
  opacity: .5;
  width: 100%;
  height: 100%;
  z-index: 10;
`;

export const Table = styled.table`
  z-index: 11;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  width: 20em;
  background: #fff;
  tr:nth-child(even) {
    background: #f1eeee;
  }
  th {
    width: 8em;
    padding: 1em;
    text-align: left;
  }
`;