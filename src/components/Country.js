import React from 'react';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import GlobalStyle from '../styled/Globals';
import { Loading, Failed } from '../styled/Loading';
import { Overlay, Table } from '../styled/Country';

export const GET_COUNTRY = gql`
  query ($code: String) {
    country(code: $code) {
      currency
      phone
      continent {
        name
        code
      }
    }
  }
`;

function Country({ history, match }) {
  const code = match.params.code;
  const { loading, error, data } = useQuery(GET_COUNTRY, {
    variables: { code }
  });

  if (loading) return <Loading className='loading'>Loading...</Loading>;

  document.body.classList.add('overflow');

  const close = () => {
    document.body.classList.remove('overflow');
    history.goBack();
  }

  return (
    <div className='container'>
      <Overlay className='overlay' onClick={close}></Overlay>
      {
        (error) 
        ? 
        <Failed className='error'>{error.message}</Failed> 
        : 
        <Table>
          <tbody>
            <tr>
              <th>Currency</th><td>{data.country.currency}</td>
            </tr>
            <tr>
              <th>Country Code</th><td>{data.country.phone}</td>
            </tr>
            <tr>
              <th>Continent</th><td>{data.country.continent.name} ({data.country.continent.code})</td>
            </tr>
          </tbody>
        </Table>
      }
      <GlobalStyle />
    </div>
  );
}

export default Country;