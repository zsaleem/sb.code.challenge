import React, { useState } from 'react';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { Link, Route } from 'react-router-dom';
import Languages from './Languages';
import Country from './Country';

import GlobalStyle from '../styled/Globals';
import { Loading, Failed } from '../styled/Loading';
import {
  Items,
  Title,
  Continent,
  CountriesContainer,
  Links,
  CountryName,
  CountryFlag,
  Back,
} from '../styled/Countries';

const GET_COUNTRIES = gql`
  {
    countries {
      name
      code
      native
      languages {
        name
        native
      }
      continent {
        name
      }
    }
  }
`;

function Countries({ match }) {
  const [continents] = useState([
    'Africa',
    'Asia',
    'Europe',
    'South America',
    'North America',
    'Oceania',
    'Antarctica'
  ]);

  const { loading, error, data } = useQuery(GET_COUNTRIES);

  if (loading) return <Loading className='loading'>Loading...</Loading>;
  if (error) return <Failed className='error'>{error.message}</Failed>;

  return(
    <div>
      <div className="container">
        <Back to='/'>Back</Back>
        {continents.map(continent => (
          <Items key={continent}>
            <Title>{continent}</Title>
            <Continent className={continent.toLowerCase()}>
              <CountriesContainer>
              {data.countries.map((item, index) => (
                <div key={item.code}>
                  {(item.continent.name === continent)
                    ?
                    <p>
                      <Links to={`${match.path}/${item.code}`}>
                        <CountryName>{item.name}({item.native})</CountryName>
                        <Languages languages={item.languages} />
                      </Links>
                    </p>
                    :
                    <div></div>
                  }
                </div>
              ))}
              </CountriesContainer>
            </Continent>
          </Items>
        ))}
        <GlobalStyle />
        <Route path={`${match.path}/:code`} component={Country} />
      </div>
    </div>
  );
}

export default Countries;