import React, { Fragment } from 'react';
import styled from 'styled-components';

const LanguageItem = styled.span`
  display: block;
`;

const Languages = ({ languages }) => (
  <span className='languages'>
    {languages.map(language => (
      (language.name != null)
        ?
        <LanguageItem key={language.name}>{language.name}({language.native})</LanguageItem>
        :
        <LanguageItem key={language.name}></LanguageItem>
    ))}
  </span>
);

export default Languages;