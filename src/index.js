import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Country from './components/Country';
import Countries from './components/Countries';

const client = new ApolloClient({
  uri: 'https://countries.trevorblades.com/'
});

ReactDOM.render(
  <div>
    <ApolloProvider client={client}>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={App} />
          <Route path='/countries' component={Countries} />
          <Route path='/countries/:id' component={Country} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  </div>, document.getElementById('root'));
